<?php

require_once('animal.php');

class Ape extends Animal
{
    public $legs = 2;

    public function __construct($string)
    {
        $this->name = $string;
    }

    public function yell()
    {
        echo "Yell : Auooo";
    }
}
